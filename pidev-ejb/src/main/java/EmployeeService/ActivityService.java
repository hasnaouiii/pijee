package EmployeeService;


import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import Entities.Activity;
import Entities.Employee;
import Entities.Status;
import RemoteService.IActivityService;




@Stateless
@LocalBean

public class ActivityService implements IActivityService{

	
	@PersistenceContext
	EntityManager em;

	@Override
	public void addActivity(Activity activity) {
		em.persist(activity);
		
	}

	@Override
	public void assignActivity(int actID, int empId) {
		Activity activityManagedEntity = em.find(Activity.class, actID);
		Employee employeManagedEntity = em.find(Employee.class, empId);
		
		activityManagedEntity.setEmployee(employeManagedEntity);
		
		em.merge(activityManagedEntity);
		
	}
	@Override
	public List<Employee> getAllEmployes() {
		List<Employee> emp = em.createQuery("Select e from Employee e", Employee.class).getResultList();
		return emp;
	}

	@Override
	public void updateActivityStatusbyID(int actID, Status status) {
		Activity activity = em.find(Activity.class, actID);
		activity.setStatus(status);
		
	}
	
	@Override
	public void deleteActivitybyID(int actID) {
		em.remove(em.find(Activity.class, actID));
		
	}

	@Override
	public void updateActivity(Activity activity) {
		Activity act=em.find(Activity.class, activity.getId());
		act.setCategory(activity.getCategory());
		act.setDescription(activity.getDescription());
		act.setFinishDate(activity.getFinishDate());
		act.setStartDate(activity.getStartDate());
		act.setStatus(activity.getStatus());
		
	}

	@Override
	public List<Activity> getAllActivities() {
		List<Activity> act = em.createQuery("Select e from Activity e", Activity.class).getResultList();
		
		return act;
	}
	@Override
	public List<Activity> getAllActivitiess() {
		List<Activity> act = em.createQuery("Select e from Activity e", Activity.class).getResultList();
		List<Activity> activities=new ArrayList<>();
		for(Activity act1 : act){
			Activity act2 = new Activity(act1.getId(), act1.getHours(),act1.getDescription(), act1.getStatus(), act1.getCategory(), act1.getStartDate(), act1.getFinishDate());
			activities.add(act2);
		}
		
		return activities;
	}
	@Override
	public List<Activity> getActivitiesByEmpID(int empID) {
		Employee employeeManagedEntity = em.find(Employee.class, empID);
		List<Activity> activities = new ArrayList<>();
		for(Activity act : employeeManagedEntity.getActivities()){
			activities.add(act);
		}
		
		return activities;
	}
	

	@Override
	public int getemployeebyname(String empName) {
		TypedQuery<Employee> query =em.createQuery("SELECT c FROM Employee c WHERE c.nom=:empName", Employee.class); 
		query.setParameter("empName", empName);
		
		Employee emp  = null;
		try { emp = query.getSingleResult();  }
		catch (Exception e)
		{ System.out.println("Erreur : " + e); }

		
		return emp.getId();
	}

	
	
	
	
	
	
	
	
	
	
	
}
