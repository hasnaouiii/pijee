package EmployeeService;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import Entities.Employee;
import Entities.Evaluation;
import RemoteService.EvaluationServiceRemote;

@Stateless
@LocalBean
public class EvaluationService implements EvaluationServiceRemote{
	
	
	
	
	
	@PersistenceContext
	EntityManager em;
	
	@Override
	public List<Evaluation> getAllEvaluation() {
		List<Evaluation> evaluations = em.createQuery("Select e from Evaluation e", Evaluation.class).getResultList();
		return evaluations;
	}
	

	@Override
	public List<Evaluation> getmyEvaluation(int id_user) {
		
		
		TypedQuery<Evaluation> query = 
				em.createQuery("select e from Evaluation e WHERE e.employees_id=:id_user ", Evaluation.class); 
				query.setParameter("id_user", id_user); 
				
				List<Evaluation> evaluations = null; 
				try {
					evaluations = query.getResultList(); 
				}
				catch (Exception e) {
					System.out.println("Erreur : " + e);
				}
				
		return evaluations;
	}
	@Override
	
	public List<Evaluation> getvalEvaluation() {
		
		
		TypedQuery<Evaluation> query = 
				em.createQuery("select e from Evaluation e WHERE e.isvalid= FALSE ", Evaluation.class); 
				 
				
				List<Evaluation> evaluations = null; 
				try {
					evaluations = query.getResultList(); 
				}
				catch (Exception e) {
					System.out.println("Erreur : " + e);
				}
				
		return evaluations;
	}
	@Override
	public int addEvaluation(Evaluation Evaluation) {
		
		em.persist(Evaluation);
		//Evaluation emp = em.find(Evaluation.class, Evaluation.getId()); 
		//emp.setEmployees(Evaluation.getEmployees());
		return Evaluation.getId();

	//public void addEvaluation(Evaluation Evaluation) {
		//Employee userManagedEntity = em.find(Employee.class, empID);
		//Evaluation.setE
	//	em.persist(Evaluation);	
		
	}

	@Override
	public void updateEvaluation(Evaluation Evaluation) {
		Evaluation emp = em.find(Evaluation.class, Evaluation.getId()); 
		
		emp.setQuestion1(Evaluation.getQuestion1()); 
		emp.setQuestion2(Evaluation.getQuestion2());
		emp.setQuestion3(Evaluation.getQuestion3()); 
		emp.setQuestion4(Evaluation.getQuestion4());
		emp.setQuestion5(Evaluation.getQuestion5()); 
		emp.setType_comptence(Evaluation.getType_comptence());
		
	}
	
	@Override
	public void updatevalid(Evaluation Evaluation) {
		Evaluation emp = em.find(Evaluation.class, Evaluation.getId()); 
		
	emp.setIsvalid(true); 
	}
	@Override
	public void fillEvaluation(Evaluation Evaluation) {
		Evaluation emp = em.find(Evaluation.class, Evaluation.getId()); 
		
		emp.setNote1(Evaluation.getNote1()); 
		emp.setNote2(Evaluation.getNote2());
		emp.setNote3(Evaluation.getNote3()); 
		emp.setNote4(Evaluation.getNote4());
		emp.setNote5(Evaluation.getNote5()); 
		
		
	}
	
	@Override
	public void deleteEvaluationbyID(int valId) {
		em.remove(em.find(Evaluation.class, valId));
	}


	

	@Override
	public List<Employee> getEvaluationByEmpID(int empID) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	
	
	
	

}
