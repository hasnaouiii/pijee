package EmployeeService;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import Entities.Cv;
import LocalService.ICvLocal;
import RemoteService.ICvService;




@Stateless
@LocalBean
public class CvService implements ICvService , ICvLocal{
	
	@PersistenceContext
	EntityManager em;
	
	
	
	@Override
	public int ajouterCv(Cv cv) {
		em.persist(cv);
		return cv.getId();
	}

	@Override
	public List<Cv> getAllCv() {
		List<Cv> emp = em.createQuery("Select e  from Cv e", Cv.class).getResultList();
		return emp;
	}

	@Override
	public void updateCv(Cv cv) {
		// TODO Auto-generated method stub
		Cv emp = em.find(Cv.class, cv.getId()); 
		emp.setNomCv(cv.getNomCv()); 
		emp.setLanguage(cv.getLanguage()); 
		emp.setProject(cv.getProject());
		emp.setSkills(cv.getSkills()); 
	
		
	}

	@Override
	public Cv getCvById(int id) {
		TypedQuery<Cv> query = 
				em.createQuery("select e from Cv e WHERE e.id=:id", Cv.class); 
				query.setParameter("id", id); 
			   Cv cv = null; 
				try {
					cv = query.getSingleResult(); 
				}
				catch (Exception e) {
					System.out.println("Erreur : " + e);
				}
				return cv;
	}

	

}
