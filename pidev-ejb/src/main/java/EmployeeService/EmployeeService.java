package EmployeeService;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import Entities.Cv;
import Entities.Employee;
import Entities.Role;
import LocalService.IEmployeLocal;
import RemoteService.IEmployeeService;


@Stateless
@LocalBean
public class EmployeeService implements IEmployeeService , IEmployeLocal{

	@PersistenceContext
	EntityManager em;
	
	@Override
	public int ajouterEmploye(Employee employe ,Cv cv) {
		employe.setCv(cv);
		em.persist(employe);
		return employe.getId();
	
	}
	
	
	
	

	@Override
	public void deleteEmployeById(int employeId) {
		em.remove(em.find(Employee.class, employeId));
		
	}

	@Override
	public Employee findUserById(String nom) {
		Employee found = null;
		String jpql = "select u from Employee u where u.nom=:nom";
		TypedQuery<Employee> query = em.createQuery(jpql, Employee.class);
		query.setParameter("nom", nom);
		try {
			found = query.getSingleResult();
		} catch (Exception ex) {
			Logger.getLogger(EmployeeService.class.getName()).log(Level.WARNING,
					"no such nom=" + nom);
		}
		return found;
	}
	
	@Override
	public void deleteUser(Employee u) {
		em.remove(em.merge(u));
			
	}
	
	
	@Override
	public Employee getEmployeByEmailAndPassword(String email, String password) {
		TypedQuery<Employee> query = 
				em.createQuery("select e from Employee e WHERE e.email=:email and e.password=:password ", Employee.class); 
				query.setParameter("email", email); 
				query.setParameter("password", password); 
				Employee employe = null; 
				try {
					employe = query.getSingleResult(); 
				}
				catch (Exception e) {
					System.out.println("Erreur : " + e);
				}
				return employe;
	}

	@Override
	public List<Employee> getAllEmployes() {
		List<Employee> emp = em.createQuery("Select e from Employee e ", Employee.class).getResultList();
		return emp;
	}
	public List<Cv> getAllCv() {
		List<Cv> emp = em.createQuery("Select e  from Cv e", Cv.class).getResultList();
		return emp;
	}

	@Override
	public void updateEmploye(Employee employe) {

		Employee emp = em.find(Employee.class, employe.getId()); 
		emp.setPrenom(employe.getPrenom()); 
		emp.setNom(employe.getNom()); 
		emp.setEmail(employe.getEmail()); 
		emp.setTeam(employe.getTeam());
		emp.setPassword(employe.getPassword()); 
		emp.setIsActif(employe.getIsActif()); 
		emp.setRole(employe.getRole());
		emp.setCv(employe.getCv());
		
		
	}
	
	public Cv getCvByName(String nomCv) {
		TypedQuery<Cv> query =em.createQuery("SELECT c FROM Cv c WHERE c.nomCv=:nomCv", Cv.class); 
		query.setParameter("nomCv", nomCv);
		
		Cv pr  = null;
		try { pr = query.getSingleResult();  }
		catch (Exception e)
		{ System.out.println("Erreur : " + e); }

		return pr;
	}

	@Override
	public Long ChartUser(Role role) {
		// TODO Auto-generated method stub
		Query query = em.createQuery("select count(*) from Employee where role=:role");
		query.setParameter("role", role);
		Long count = (Long) query.getSingleResult();
		return count;
	}

	@Override
	public Employee getEmByNom(String nom) {
		TypedQuery<Employee> query = 
				em.createQuery("select e from Employee e WHERE e.nom=:nom", Employee.class); 
				query.setParameter("nom", nom); 
				Employee employee = null; 
				try {
					employee = query.getSingleResult(); 
				}
				catch (Exception e) {
					System.out.println("Erreur : " + e);
				}
				return employee;
	}
	
	

}
