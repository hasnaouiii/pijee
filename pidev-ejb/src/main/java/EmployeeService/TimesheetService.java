package EmployeeService;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import Entities.Activity;
import Entities.Employee;
import Entities.Timesheet;
import RemoteService.ITimesheetService;


@Stateless
@LocalBean
public class TimesheetService implements ITimesheetService {
	@PersistenceContext
	EntityManager em;

	@Override
	public void addTimesheet(Timesheet ts, int EmpID) {
	
	em.persist(ts);
	Employee employeManagedEntity = em.find(Employee.class, EmpID);
	Timesheet timesheetManagedEntity = em.find(Timesheet.class, ts.getId());
	ts.setEmployees(employeManagedEntity);
	em.merge(timesheetManagedEntity);
		
	}

	@Override
	public void updateTimesheet(int TimeID, int workhour) {
		Timesheet timesheetManagedEntity = em.find(Timesheet.class, TimeID);
		timesheetManagedEntity.setWorkingHours(workhour);
		
	}

	@Override
	public List<Timesheet> getTimesheetbyEmp(int empID) {
		Employee employeeManagedEntity = em.find(Employee.class, empID);
		List<Timesheet> timesheets = new ArrayList<>();
		for(Timesheet ts : employeeManagedEntity.getTimesheets()){
			timesheets.add(ts);
		}
		
		return timesheets;
	}

	@Override
	public String CalculHoursbyAct(int ActId, int EmpID) {
		int HourByAct=0;
		List<Timesheet> ts = getTimesheetbyEmp(EmpID);
		for(Timesheet ts1 : ts){
			if (ts1.getActivityID() == ActId)
			{
				HourByAct= HourByAct+ts1.getWorkingHours();
			}
		}
		Activity act=em.find(Activity.class,ActId);
		if(HourByAct <= act.getHours())
			
		{	
			int difference=act.getHours()-HourByAct;
			return "Congrats You made it in "+ HourByAct+" and  "+difference+" before the deadline";
		}
		else 
		{ 	
			int difference=HourByAct-act.getHours();
			return " You can do better :( you made it in "+ HourByAct+" and "+difference+" after the deadline";
		}
		
		
	}

	

	@Override
	public void deleteTimesheet(int timeID) {
		Timesheet timesheetManagedEntity = em.find(Timesheet.class, timeID);
		em.remove(timesheetManagedEntity);
		
	}
}
