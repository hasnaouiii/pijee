package RemoteService;

import java.util.List;

import javax.ejb.Remote;

import Entities.Employee;
import Entities.Evaluation;



@Remote
public interface EvaluationServiceRemote {

	public List<Evaluation> getAllEvaluation();
	public List<Evaluation> getmyEvaluation(int id_user);
	public List<Evaluation> getvalEvaluation();
	public int addEvaluation(Evaluation Evaluation);
	public void updateEvaluation(Evaluation Evaluation);
	public void updatevalid(Evaluation Evaluation);
	public void fillEvaluation(Evaluation Evaluation);
	public void deleteEvaluationbyID(int valId);
	public List<Employee> getEvaluationByEmpID(int empID);
	
	
	
	
	
	
	
	
	
	
	
	
	
}
