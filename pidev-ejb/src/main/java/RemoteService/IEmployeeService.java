package RemoteService;



import java.util.List;

import javax.ejb.Remote;

import Entities.Cv;
import Entities.Employee;
import Entities.Role;

@Remote
public interface IEmployeeService {
	
	public int ajouterEmploye(Employee employe , Cv cv);
	public void deleteEmployeById(int employeId);
	public Employee getEmployeByEmailAndPassword(String email, String password); 
	public List<Employee> getAllEmployes();
	public void updateEmploye(Employee employe);
	public Long ChartUser( Role role);
	
	

}
