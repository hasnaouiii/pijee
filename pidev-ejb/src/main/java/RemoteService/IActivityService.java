package RemoteService;

import java.util.List;

import javax.ejb.Remote;

import Entities.Activity;
import Entities.Employee;
import Entities.Status;


@Remote
public interface IActivityService {
	public void addActivity(Activity activity);
	public void assignActivity(int actID,int empId);
	public void updateActivity(Activity activity);
	public void updateActivityStatusbyID(int actID, Status status);
	public void deleteActivitybyID(int actID);
	public List<Activity> getAllActivities();
	public List<Activity> getAllActivitiess();
	public List<Activity> getActivitiesByEmpID(int empID);
	public int getemployeebyname(String empName);
	public List<Employee> getAllEmployes();
}
