package RemoteService;

import java.util.List;

import javax.ejb.Remote;

import Entities.Timesheet;

@Remote
public interface ITimesheetService {

	public void addTimesheet(Timesheet ts , int EmpID);
	public void updateTimesheet(int TimeID,int workhour);
	public void deleteTimesheet(int timeID);
	public List<Timesheet> getTimesheetbyEmp(int empID);
	public String CalculHoursbyAct(int ActId,int EmpID);
	
	
}
