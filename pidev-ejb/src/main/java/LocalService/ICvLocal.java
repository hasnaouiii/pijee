package LocalService;

import java.util.List;

import javax.ejb.Local;

import Entities.Cv;



@Local
public interface ICvLocal {
	public List<Cv> getAllCv();
	public Cv getCvById(int id); 
	public void updateCv(Cv cv);
}
