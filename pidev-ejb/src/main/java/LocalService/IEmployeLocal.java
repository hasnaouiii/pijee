package LocalService;

import java.util.List;

import javax.ejb.Local;


import Entities.Cv;
import Entities.Employee;
import Entities.Role;

@Local
public interface IEmployeLocal {
	
	public int ajouterEmploye(Employee employe , Cv cv);
	public void deleteEmployeById(int employeId);
	public Employee getEmployeByEmailAndPassword(String email, String password); 
	public Employee getEmByNom(String nom);
	public void deleteUser(Employee u);
	public Employee findUserById(String cin);
	public List<Employee> getAllEmployes();
	public void updateEmploye(Employee employe);
	public Long ChartUser( Role role);
	
	

}
