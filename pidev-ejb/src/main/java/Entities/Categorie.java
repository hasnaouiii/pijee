package Entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;


@Entity
public class Categorie implements Serializable {
	
private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_cat;
	private String nom;
	private String description;
	
	@ManyToMany
	private List<Employee> employe;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="categorie")
	private List<Competence> listcom;

	public Categorie(int id_cat, String nom, String description, List<Employee> employe, List<Competence> listcom) {
		super();
		this.id_cat = id_cat;
		this.nom = nom;
		this.description = description;
		this.employe = employe;
		this.listcom = listcom;
	}

	public Categorie(String nom, String description, List<Employee> employe, List<Competence> listcom) {
		super();
		this.nom = nom;
		this.description = description;
		this.employe = employe;
		this.listcom = listcom;
	}
	
	

	public Categorie() {
		super();
	}

	/**
	 * @return the id_cat
	 */
	public int getId_cat() {
		return id_cat;
	}

	/**
	 * @param id_cat the id_cat to set
	 */
	public void setId_cat(int id_cat) {
		this.id_cat = id_cat;
	}

	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the employee
	 */
	public List<Employee> getEmployee() {
		return employe;
	}

	/**
	 * @param employee the employee to set
	 */
	public void setEmployee(List<Employee> employe) {
		this.employe = employe;
	}

	/**
	 * @return the listcom
	 */
	public List<Competence> getListcom() {
		return listcom;
	}

	/**
	 * @param listcom the listcom to set
	 */
	public void setListcom(List<Competence> listcom) {
		this.listcom = listcom;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Categorie [id_cat=" + id_cat + ", nom=" + nom + ", description=" + description + ", employee="
				+ employe + ", listcom=" + listcom + "]";
	}



	


}
