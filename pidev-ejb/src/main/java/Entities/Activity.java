package Entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Activity implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private int hours;
	private String description;
	@Enumerated(EnumType.STRING)
	private Status status;
	private String category;
	@Temporal (TemporalType.DATE)
	private Date startDate;
	@Temporal (TemporalType.DATE)
	private Date finishDate;
	@ManyToOne
	private Employee employee;
	
	
	
	
	public Activity() {
		super();
	}
	
	
	
	



	public Activity(int hours,String description, Status status, String category, Date startDate, Date finishDate) {
		
		this.hours=hours;
		this.description = description;
		this.status = status;
		this.category = category;
		this.startDate = startDate;
		this.finishDate = finishDate;
	}
	public Activity(int id,int hours,String description, Status status, String category, Date startDate, Date finishDate) {
		this.id=id;
		this.hours=hours;
		this.description = description;
		this.status = status;
		this.category = category;
		this.startDate = startDate;
		this.finishDate = finishDate;
	}







	public Date getStartDate() {
		return startDate;
	}







	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}







	public Date getFinishDate() {
		return finishDate;
	}







	public void setFinishDate(Date finishDate) {
		this.finishDate = finishDate;
	}







	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}



	public Employee getEmployee() {
		return employee;
	}



	public void setEmployee(Employee employee) {
		this.employee = employee;
	}







	public int getHours() {
		return hours;
	}







	public void setHours(int hours) {
		this.hours = hours;
	}
	
	


}
