package Entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;




@Entity


public class Employee  implements Serializable {
	
	private static final long serialVersionUID = 1L;
 
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String prenom;
	private String nom;
	private String email;
	private String team;
	private Boolean isActif;
	private String password;
	@Enumerated(EnumType.STRING)
	private Role role;
	
	
	@OneToMany(mappedBy="employee",cascade = CascadeType.ALL)
	@JsonBackReference
	private List<Activity> activities;
	
	@OneToMany(mappedBy="employees")
	@JsonBackReference
	private List<Evaluation> Evaluation;
	

	@OneToMany(mappedBy="employees")
	@JsonBackReference
	private List<conge> conge;
	
	@ManyToMany(mappedBy ="employe")
	@JsonBackReference
	private List<Categorie> categorie; 
	
	
	public Employee() {
		super();
	}

	
	
	
	




	public Employee(int id, String prenom, String nom, String email, String team, Boolean isActif, String password,
			Role role, Cv cv) {
		 
		this.id = id;
		this.prenom = prenom;
		this.nom = nom;
		this.email = email;
		this.team = team;
		this.isActif = isActif;
		this.password = password;
		this.role = role;
		this.cv = cv;
	}




	public Employee(String prenom, String nom, String email, String team, Boolean isActif, String password, Role role ) {
		super();
		this.prenom = prenom;
		this.nom = nom;
		this.email = email;
		this.team = team;
		this.isActif = isActif;
		this.password = password;
		this.role = role;
		
	}




	public Employee(int id, String prenom, String nom, String email, String team, Boolean isActif, String password,
			Role role) {
		super();
		this.id = id;
		this.prenom = prenom;
		this.nom = nom;
		this.email = email;
		this.team = team;
		this.isActif = isActif;
		this.password = password;
		this.role = role;
	}


	@OneToMany(mappedBy="employees" )
	@JsonBackReference
	private List<Timesheet> timesheets;


	public String getTeam() {
		return team;
	}
	public void setTeam(String team) {
		this.team = team;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	
	
	public Boolean getIsActif() {
		return isActif;
	}




	public void setIsActif(Boolean isActif) {
		this.isActif = isActif;
	}




	public Cv getCv() {
		return cv;
	}




	public void setCv(Cv cv) {
		this.cv = cv;
	}


	@OneToOne
	private Cv cv;

	public List<Activity> getActivities() {
		return activities;
	}
	
	public void setActivities(List<Activity> activities) {
		this.activities = activities;
	}
	
	public List<Timesheet> getTimesheets() {
		return timesheets;
	}




	public void setTimesheets(List<Timesheet> timesheets) {
		this.timesheets = timesheets;
	}




	@Override
	public String toString() {
		return "Employee [id=" + id + ", prenom=" + prenom + ", nom=" + nom + ", email=" + email + ", team=" + team
				+ ", isActif=" + isActif + ", password=" + password + ", role=" + role + ", cv=" + cv + "]";
	}




	


	
	
	
	
}
