package Entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class conge  implements Serializable  {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String nom;
	private String cause;
	private Date dateDebut;
	private Date dateFin;
	@ManyToOne()
	@JoinColumn(name = "employees_id", referencedColumnName = "id")
	private Employee employees;

	public conge() {
		super();
	}
	
	
	
	
	public conge(int id, Date dateDebut, Date dateFin, String nom, String cause) {
		super();
		this.id = id;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
		this.nom = nom;
		this.cause = cause;
	}




	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getDateDebut() {
		return dateDebut;
	}
	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}
	public Date getDateFin() {
		return dateFin;
	}
	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}




	public String getNom() {
		return nom;
	}




	public void setNom(String nom) {
		this.nom = nom;
	}




	public String getCause() {
		return cause;
	}




	public void setCause(String cause) {
		this.cause = cause;
	}




	@Override
	public String toString() {
		return "conge [id=" + id + ", dateDebut=" + dateDebut + ", dateFin=" + dateFin + ", nom=" + nom + ", cause="
				+ cause + "]";
	}
	
	
	
	
	
}
