package Entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Evaluation  implements Serializable{
	private static final long serialVersionUID = 1L;
	 
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String Question1;
	private String Question2;
	private String Question3;
	private String Question4;
	private String Question5;
	@Temporal (TemporalType.DATE)
	private Date date;
	@Enumerated(EnumType.STRING)
	private Type_comptence Type_comptence;
	@ManyToOne()
	@JoinColumn(name = "employees_id", referencedColumnName = "id")
	private Employee employees;
	private boolean isvalid ;
	


	public void setIsvalid(boolean isvalid) {
		this.isvalid = isvalid;
	}


	public boolean isIsvalid() {
		return isvalid;
	}


	@Enumerated(EnumType.STRING)
	private NOTEevaluation note1;
	@Enumerated(EnumType.STRING)
	private NOTEevaluation note2;
	@Enumerated(EnumType.STRING)
	private NOTEevaluation note3;
	@Enumerated(EnumType.STRING)
	private NOTEevaluation note4;
	@Enumerated(EnumType.STRING)
	private NOTEevaluation note5;
	//@Column( updatable = false, insertable = false)
  //  private int employees_id ;
	//public int getEmployees_id() {
	//	return employees_id;
	//}


	//public void setEmployees_id(int employees_id) {
	//	this.employees_id = employees_id;
	//}


	public Evaluation() 
		// TODO Auto-generated constructor stub
	 {
		super();
	}

	
	public Evaluation(String Question1, String Question2, String Question3, String Question4, String Question5 ,Date date, Type_comptence Type_comptence) {
		super();
		
		this.Question1 = Question1;
		this.Question2 = Question2;
		this.Question3 = Question3;
		this.Question4 = Question4;
		this.Question5 = Question5;

		this.date = date;
		
		this.Type_comptence = Type_comptence;
		
		
	}
	public Evaluation(int id,NOTEevaluation note1, NOTEevaluation note2, NOTEevaluation note3, NOTEevaluation note4, NOTEevaluation note5 ) {
		super();
		this.id = id;
		this.note1 = note1;
		this.note2 = note2;
		this.note3 = note3;
		this.note4 = note4;
		this.note5 = note5;

		
		
	}
	public Evaluation(int id,Boolean isvalid ) {
		super();
		this.id = id;
		this.isvalid = isvalid;
		
		
	}
	public NOTEevaluation getNote1() {
		return note1;
	}


	public void setNote1(NOTEevaluation note1) {
		this.note1 = note1;
	}


	public NOTEevaluation getNote2() {
		return note2;
	}


	public void setNote2(NOTEevaluation note2) {
		this.note2 = note2;
	}


	public NOTEevaluation getNote3() {
		return note3;
	}


	public void setNote3(NOTEevaluation note3) {
		this.note3 = note3;
	}


	public NOTEevaluation getNote4() {
		return note4;
	}


	public void setNote4(NOTEevaluation note4) {
		this.note4 = note4;
	}


	public NOTEevaluation getNote5() {
		return note5;
	}


	public void setNote5(NOTEevaluation note5) {
		this.note5 = note5;
	}


	public Evaluation(int id,String Question1, String Question2, String Question3, String Question4, String Question5 ,Date date, Type_comptence Type_comptence) {
		super();
		this.id = id;
		this.Question1 = Question1;
		this.Question2 = Question2;
		this.Question3 = Question3;
		this.Question4 = Question4;
		this.Question5 = Question5;

		this.date = date;
		
		this.Type_comptence = Type_comptence;
		
		
	}
	public Evaluation(String Question1, String Question2, String Question3, String Question4, String Question5 ,Date date, Type_comptence Type_comptence,Employee employees) {
		super();
		
		this.Question1 = Question1;
		this.Question2 = Question2;
		this.Question3 = Question3;
		this.Question4 = Question4;
		this.Question5 = Question5;

		this.date = date;
		
		this.Type_comptence = Type_comptence;
		this.employees = employees;
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getQuestion1() {
		return Question1;
	}
	public String getQuestion2() {
		return Question2;
	}
	public String getQuestion3() {
		return Question3;
	}
	public String getQuestion4() {
		return Question4;
	}
	public String getQuestion5() {
		return Question5;
	}
	public void setQuestion1(String Question1) {
		this.Question1 = Question1;
	}
	public void setQuestion2(String Question2) {
		this.Question2 = Question2;
	}
	public void setQuestion3(String Question3) {
		this.Question3 = Question3;
	}
	public void setQuestion4(String Question4) {
		this.Question4 = Question4;
	}
	public void setQuestion5(String Question5) {
		this.Question5 = Question5;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

	public Type_comptence getType_comptence() {
		return Type_comptence;
	}
	public void setType_comptence(Type_comptence Type_comptence) {
		this.Type_comptence = Type_comptence;
	}
	
	public Employee getEmployees() {
		return employees;
	}
	public void setEmployees(Employee employees) {
		this.employees = employees;
	}


	@Override
	public String toString() {
		return "Evaluation [id=" + id + ", Question1=" + Question1 + ", Question2=" + Question2 + ", Question3="
				+ Question3 + ", Question4=" + Question4 + ", Question5=" + Question5 + ", date=" + date
				+ ", Type_comptence=" + Type_comptence +", isvalid=" + isvalid + ", employees=" + employees + ", note1=" + note1 + ", note2="
				+ note2 + ", note3=" + note3 + ", note4=" + note4 + ", note5=" + note5 + "]";
	}
	
	
	
	
	

}
