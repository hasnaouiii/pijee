package Entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;



@Entity
public class Competence implements Serializable {
	
	
private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_comp;
	private String description;
	@Enumerated(EnumType.STRING)
	private Niveau niveau;
	
	
	@ManyToOne
	private Categorie categorie;


	public Competence(int id_comp, String description, Niveau niveau, Categorie categorie) {
		super();
		this.id_comp = id_comp;
		this.description = description;
		this.niveau = niveau;
		this.categorie = categorie;
	}


	public Competence(String description, Niveau niveau, Categorie categorie) {
		super();
		this.description = description;
		this.niveau = niveau;
		this.categorie = categorie;
	}


	public Competence() {
		super();
	}


	/**
	 * @return the id_comp
	 */
	public int getId_comp() {
		return id_comp;
	}


	/**
	 * @param id_comp the id_comp to set
	 */
	public void setId_comp(int id_comp) {
		this.id_comp = id_comp;
	}


	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}


	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}


	/**
	 * @return the niveau
	 */
	public Niveau getNiveau() {
		return niveau;
	}


	/**
	 * @param niveau the niveau to set
	 */
	public void setNiveau(Niveau niveau) {
		this.niveau = niveau;
	}


	/**
	 * @return the categorie
	 */
	public Categorie getCategorie() {
		return categorie;
	}


	/**
	 * @param categorie the categorie to set
	 */
	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}


	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Competence [id_comp=" + id_comp + ", description=" + description + ", niveau=" + niveau + ", categorie="
				+ categorie + "]";
	} 


}
