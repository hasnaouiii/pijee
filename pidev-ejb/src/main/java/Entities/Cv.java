package Entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;


@Entity
public class Cv implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String nomCv;
	private String skills;
	private String project;
	private String language;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSkills() {
		return skills;
	}
	public void setSkills(String skills) {
		this.skills = skills;
	}
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	
	public Cv(int id, String nomCv, String skills, String project, String language) {
		
		this.id = id;
		this.nomCv = nomCv;
		this.skills = skills;
		this.project = project;
		this.language = language;
		
	}
	
	
	public Cv(String nomCv, String skills, String project, String language) {
		super();
		this.nomCv = nomCv;
		this.skills = skills;
		this.project = project;
		this.language = language;
	}
	public String getNomCv() {
		return nomCv;
	}
	public void setNomCv(String nomCv) {
		this.nomCv = nomCv;
	}
	public Cv() {
		
	}
	
	@OneToOne(mappedBy="cv")
	private Employee employe;
	@Override
	public String toString() {
		return nomCv ;
	}
	
	
	
	
	
}
