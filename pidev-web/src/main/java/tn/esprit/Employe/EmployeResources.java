package tn.esprit.Employe;

import javax.ejb.EJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import Entities.Employee;
import Entities.Role;
import LocalService.IEmployeLocal;




@Path("employes")
public class EmployeResources {
	//liste statique
	@EJB
	IEmployeLocal useremploye;
	//(A) Ajouter un employe
		
		/*@POST
		@Consumes(MediaType.APPLICATION_JSON)
		public Response addEmployee (Employee employe) {
		
			return Response.ok(employee.getAllEmployes()).build();
		}*/
		//obtenir la liste de tous les employes$
		@GET
		@Produces(MediaType.APPLICATION_JSON)
		public Response displayeEmployeesList() {
			if (useremploye.getAllEmployes() != null)
				return Response.status(Status.FOUND).entity(useremploye.getAllEmployes()).build();
			return Response.status(Status.NOT_FOUND).build();
			
		}
		
		
		 @DELETE
			@Path("{nom}")
			@Produces(MediaType.APPLICATION_JSON)
			public Response deleteEmployee(@PathParam(value="nom")String nom) {
			 Employee u = useremploye.findUserById(nom);
			 useremploye.deleteUser(u);
				return Response.status(Status.OK).entity("user removed").build();
			}
			
		
		 @GET
			@Produces(MediaType.APPLICATION_JSON)
			@Path("{nom}")
			public Response findEmploById(@PathParam(value="nom") String nom) {
				return Response.status(Status.OK).entity(useremploye.findUserById(nom)).build();
			}
		
		 
		 @GET
			//@Path("auth")
			@Produces(MediaType.APPLICATION_JSON)
			public Response getEmployeByEmailAndPassword (@QueryParam(value="login") String login ,@QueryParam(value="password") String password) {
				
		       
			    return Response.status(Status.OK).entity(useremploye.getEmployeByEmailAndPassword(login, password)).build();
				
			} 
		
		
		 @GET
			//@Path("auth")
			@Produces(MediaType.APPLICATION_JSON)
			public Response ChartUser (@QueryParam(value="role") Role role ) {
				
		       
			    return Response.status(Status.OK).entity(useremploye.ChartUser(role)).build();
				
			} 
		
		
		
		
}
