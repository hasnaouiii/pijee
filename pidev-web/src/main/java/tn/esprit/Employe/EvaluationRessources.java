package tn.esprit.Employe;

import javax.ejb.EJB;
import javax.faces.bean.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import EmployeeService.EvaluationService;
import Entities.Evaluation;

@Path("Evaluation")
@RequestScoped
public class EvaluationRessources {
	@EJB
	 EvaluationService  EVAL;
		
		
		
		
		@GET
		@Produces(MediaType.APPLICATION_JSON)
		public Response displaylisteEvaluation () {
			return Response.status(Status.OK).entity(EVAL.getAllEvaluation()).build();
		}
		@POST
		@Consumes({ MediaType.APPLICATION_JSON })
		public Response AddEvaluation(Evaluation evl)
		{
			EVAL.addEvaluation(evl);
			return Response.status(Status.CREATED).entity("ok").build();
			
		}
		
		@DELETE
		@Path("{id}")
		public Response DeleteEvaluation(@PathParam("id")int id)
		{
			EVAL.deleteEvaluationbyID(id);
				return Response.status(Status.OK).entity("le diplome a ete supprime").build();
			
		}
		
		//@PUT
		//@Consumes({ MediaType.APPLICATION_JSON })
		//public Response UpdateCategorie(Evaluation evl)
		//{
			//EVAL.updateEvaluation(evl);;
			//return Response.status(Status.OK).entity("le diplome a ete modifie").build();
			
		//}
		@PUT
		@Path("{id}")
		@Consumes(MediaType.APPLICATION_JSON)
		public Response update(@PathParam(value="id") int id, Evaluation evl) {
			evl.setId(id);;
			try {
				EVAL.updateEvaluation(evl);
				return Response.status(Status.OK).entity("eval edited").build();
			}
			catch(Exception e){
				return Response.status(Status.NOT_FOUND).entity("lproblem").build();
			}

		}
}
