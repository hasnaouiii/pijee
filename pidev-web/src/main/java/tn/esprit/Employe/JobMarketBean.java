package tn.esprit.Employe;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

import org.primefaces.model.chart.PieChartModel;

import Entities.Role;
import RemoteService.IEmployeeService;


@ManagedBean
public class JobMarketBean {
	  private PieChartModel model;
	 
  
  @EJB
  IEmployeeService userSer;

  @PostConstruct
  public void init() {
	  Long n1= userSer.ChartUser(Role.Administartor);
	  Long n2= userSer.ChartUser(Role.Employe);
	  Long n3= userSer.ChartUser(Role.Manager);
      model = new PieChartModel();
      model.set("Manager", n3);//jobs in thousands
      model.set("Administartor", n1);
      model.set("Employe", n2);
      
     
     // model2.set("EnConge", m4);

      //followings are some optional customizations:
      //set title
      model.setTitle("Role Users");
      //set legend position to 'e' (east), other values are 'w', 's' and 'n'
      model.setLegendPosition("e");
      //enable tooltips
      model.setShowDatatip(true);
      //show labels inside pie chart
      model.setShowDataLabels(true);
      //show label text  as 'value' (numeric) , others are 'label', 'percent' (default). Only one can be used.
      model.setDataFormat("value");
      //format: %d for 'value', %s for 'label', %d%% for 'percent'
      model.setDataLabelFormatString("%d");
      //pie sector colors
      model.setSeriesColors("aaf,afa,faa,ffa,aff,faf,ddd");
      
    
  }

  public PieChartModel getModel() {
      return model;
  }
}