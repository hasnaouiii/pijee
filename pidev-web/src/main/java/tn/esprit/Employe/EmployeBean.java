package tn.esprit.Employe;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import EmployeeService.EmployeeService;
import Entities.Cv;
import Entities.Employee;
import Entities.Role;






@ManagedBean(name = "employeBean")
@SessionScoped 
public class EmployeBean  implements Serializable {
	private static final long serialVersionUID = 1L;
	private String prenom;  private String nom; 
	private String password;
	private String email;
	private String nomCv;
	private String team;
	private List<Employee> employes;
	private List<Cv> cvs;
	private Boolean isActif; 
	private Integer employeIdToBeUpdated;
	private Role role;  
	private Cv cv;
	
	@EJB
	EmployeeService employeService; 
	public String addEmploye() { 
		cv = employeService.getCvByName(nomCv);
    	System.out.println(nomCv);
    	System.out.println(cv+"--");
		
	employeService.ajouterEmploye(new Employee(prenom, nom, email, team, isActif, password, role),cv);
	this.setPrenom(null);
	this.setNom(null);
	this.setEmail(null);
	this.setTeam(null);
	this.setIsActif(null);
	this.setRole(null);
	this.setNomCv(null);
	return"/pages/admin/ADDEMPL?faces-redirect=true";		}
	
	
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	
	public String getNomCv() {
		return nomCv;
	}
	public void setNomCv(String nomCv) {
		this.nomCv = nomCv;
	}


	
	public String getNom() {
		return nom;
	}
	
	public Integer getEmployeIdToBeUpdated() {
		return employeIdToBeUpdated;
	}
	public void setEmployeIdToBeUpdated(Integer employeIdToBeUpdated) {
		this.employeIdToBeUpdated = employeIdToBeUpdated;
	}
	public void setEmployes(List<Employee> employes) {
		this.employes = employes;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	
	
	public String getTeam() {
		return team;
	}
	public void setTeam(String team) {
		this.team = team;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Boolean getIsActif() {
		return isActif;
	}
	public void setIsActif(Boolean isActif) {
		this.isActif = isActif;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	
	
	public Cv getCv() {
		return cv;
	}
	public void setCv(Cv cv) {
		this.cv = cv;
	}
	public EmployeeService getEmployeService() {
		return employeService;
	}
	public void setEmployeService(EmployeeService employeService) {
		this.employeService = employeService;
	}
	
	
	public List<Employee> getEmployes() { employes = employeService.getAllEmployes(); return employes; }
	
	
	public void removeEmploye(int employeId) { employeService.deleteEmployeById(employeId); }
	
	public String updateEmploye() { 
		employeService.updateEmploye(new Employee(employeIdToBeUpdated,prenom, nom,email,team, isActif,password , role ,cv)); 
		this.setPrenom(null);
		this.setNom(null);
		this.setEmail(null);
		this.setTeam(null);
		this.setIsActif(null);
		this.setRole(null);
		this.setNomCv(null);
		return"/pages/admin/AFFEMPL?faces-redirect=true";
	} 
	
	public void displayEmploye(Employee empl) {
	this.setPrenom(empl.getPrenom()); 
	this.setNom(empl.getNom());
	this.setIsActif(empl.getIsActif()); 
	this.setEmail(empl.getEmail());
	this.setRole(empl.getRole()); 
	this.setEmail(empl.getEmail());
	this.setTeam(empl.getTeam());
	this.setPassword(empl.getPassword());
	this.setCv(empl.getCv());
	this.setEmployeIdToBeUpdated(empl.getId()); }
	
	public List<Cv> getCvs() {
		cvs = employeService.getAllCv(); return cvs;
	}

}
