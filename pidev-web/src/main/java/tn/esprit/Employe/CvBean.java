package tn.esprit.Employe;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import EmployeeService.CvService;
import Entities.Cv;




@ManagedBean(name = "cvBean")
@SessionScoped 
public class CvBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String nomCv;
	private String skills;
	private String project;
	private String language;
	private Integer cvIdToBeUpdated;
	private List<Cv> cvs;
	
	@EJB
	CvService cvService; 
	public String addCv() { 
		cvService.ajouterCv(new Cv(nomCv, skills, project, language));
		this.setNomCv(null);
		this.setSkills(null);
		this.setProject(null);
		this.setLanguage(null);
		return"/pages/admin/ADDCV?faces-redirect=true";
		
		}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNomCv() {
		return nomCv;
	}
	public void setNomCv(String nomCv) {
		this.nomCv = nomCv;
	}
	public String getSkills() {
		return skills;
	}
	public void setSkills(String skills) {
		this.skills = skills;
	}
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public List<Cv> getCvs() {
		cvs = cvService.getAllCv(); return cvs;
	}
	
	
	
	
	public Integer getCvIdToBeUpdated() {
		return cvIdToBeUpdated;
	}
	public void setCvIdToBeUpdated(Integer cvIdToBeUpdated) {
		this.cvIdToBeUpdated = cvIdToBeUpdated;
	}
	public void updateEmploye() {cvService.updateCv(new Cv(cvIdToBeUpdated, nomCv,skills,project,language)); }
	
	
	public void displayCve(Cv cv) {
		this.setNomCv(cv.getNomCv());
		this.setSkills(cv.getSkills()); 
		this.setLanguage(cv.getLanguage());
		this.setProject(cv.getProject()); 
		this.setCvIdToBeUpdated(cv.getId()); }
	

}
