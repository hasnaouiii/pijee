package tn.esprit.Employe;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import Entities.Cv;
import LocalService.ICvLocal;





@Path("cvs")
public class CvResources {
	
	
	@EJB
	ICvLocal cvList;
	
	
	
	
	
	
	

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response displayeEmployeesList() {
		if (cvList.getAllCv() != null)
			return Response.status(Status.FOUND).entity(cvList.getAllCv()).build();
		return Response.status(Status.NOT_FOUND).build();
		
	}

	
	/*@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchEmployee1(@QueryParam(value="nomCv") int id) {
		if(id==0) {return Response.status(Status.OK).entity(cvList.getAllCv()).build();}
		else {
		Iterator<Cv> itr = cvList.getAllCv().iterator();
		Cv result = null;
	    while (itr.hasNext() && result == null) {
	    	Cv temp = itr.next();
	        if(temp.getId()==id) {
	        	result = temp;
	        }
	    }
		if(result != null) {
			return Response.status(302).entity(result).build();
		}
		return Response.status(Status.NOT_FOUND).entity("cv non trouvé").build();}
	}*/
	
	
	 @GET
		@Produces(MediaType.APPLICATION_JSON)
		@Path("{id}")
		public Response findCvById(@PathParam(value="id") int id) {
			return Response.status(Status.OK).entity(cvList.getCvById(id)).build();
		}
	
	
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("Edit")
	public Response updateTest(Cv t) {
		 cvList.updateCv(t);
		return Response.status(Status.OK).entity(t).build();
	}
	
	
	
	// <Updtate EMploye
				@PUT
				@Path("{id}")
				@Produces(MediaType.APPLICATION_JSON)
				@Consumes(MediaType.APPLICATION_JSON)
				public Response  UpdateEmploye(@PathParam(value="id")int id,Cv e)
				{
				for(int i=0;i< cvList.getAllCv().size();i++) {
					if( cvList.getAllCv().get(i).getId() == id) {
						cvList.getAllCv().get(i).setNomCv(e.getNomCv());
						cvList.getAllCv().get(i).setLanguage(e.getLanguage());
						cvList.getAllCv().get(i).setProject(e.getProject());
						cvList.getAllCv().get(i).setSkills(e.getSkills());
					
						return Response.status(Status.ACCEPTED).entity("update succeful").build();

					}
					
				}
				return Response.status(Status.NOT_MODIFIED).entity("update failed").build();

		}
	
	
	
	
	
	
	
	
	
	

}
