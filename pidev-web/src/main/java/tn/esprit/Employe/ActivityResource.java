package tn.esprit.Employe;

import javax.ejb.EJB;
import javax.faces.bean.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import EmployeeService.ActivityService;
import EmployeeService.TimesheetService;
import Entities.Activity;


@Path("activities")
@RequestScoped
public class ActivityResource {
	@EJB
	ActivityService activityService;
	@EJB
	TimesheetService ts;
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON )
	public Response AddCategorie(Activity act)
	{
		activityService.addActivity(act);
		return Response.status(Status.CREATED).entity("Activity added").build();
		
	}
	@DELETE
	@Path("{id}")
	public Response DeleteCategorie(@PathParam("id")int id)
	{
		activityService.deleteActivitybyID(id);;
			return Response.status(Status.OK).entity("activity deleted").build();
		
	}
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response update(@QueryParam(value="actID") int actid,@QueryParam(value="EmpId") int empid,Activity act) {
		if (empid < 1) {
		act.setId(actid);
		try {
			activityService.updateActivity(act);
			return Response.status(Status.OK).entity("Activity updated").build();
		}
		catch(Exception e){
			return Response.status(Status.NOT_FOUND).entity("Activity not found").build();
		}}
		else {activityService.assignActivity(actid, empid);
		return Response.status(Status.OK).entity("Activity assigned to employee "+empid).build();}
		}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response Calcul(@QueryParam(value="actID") int actid,@QueryParam(value="EmpId") int empid,Activity act) {
		if (empid <1 || actid<1) {
			return Response.status(Status.OK).entity(activityService.getAllActivitiess()).build();
		}
		
		else {
			String suuuuu= ts.CalculHoursbyAct(actid, empid);
			return Response.status(Status.OK).entity(suuuuu).build();
			
		}
	}
}
