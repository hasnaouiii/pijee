package tn.esprit.Employe;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import EmployeeService.EmployeeService;
import Entities.Employee;
import Entities.Role;


@ManagedBean
@SessionScoped
public class LoginBean {
	private String login;
	private String password;
	private Employee employe;
	private Boolean loggedIn;

	@EJB
	EmployeeService employeService;

	public String doLogin() {
		String navigateTo= "null";
		employe= employeService.getEmployeByEmailAndPassword(login, password);
		if(employe!= null&& employe.getRole() == Role.Administartor) {
			navigateTo= "/pages/admin/index?faces-redirect=true"; loggedIn= true; }
		else if(employe!= null&& employe.getRole() == Role.Manager){
			navigateTo= "/pages/admin/index?faces-redirect=true"; loggedIn= true; 
		}
		
			else	{FacesContext.getCurrentInstance().addMessage("form:btn", new FacesMessage("Bad Credentials"));
		}
		return navigateTo; 
	}


	public String doLogout() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return"/login?faces-redirect=true";
	}


	public String getLogin() {
		return login;
	}


	public void setLogin(String login) {
		this.login = login;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public Employee getEmploye() {
		return employe;
	}


	public void setEmploye(Employee employe) {
		this.employe = employe;
	}


	public Boolean getLoggedIn() {
		return loggedIn;
	}


	public void setLoggedIn(Boolean loggedIn) {
		this.loggedIn = loggedIn;
	}


	public EmployeeService getEmployeService() {
		return employeService;
	}


	public void setEmployeService(EmployeeService employeService) {
		this.employeService = employeService;
	}
	
	
}
